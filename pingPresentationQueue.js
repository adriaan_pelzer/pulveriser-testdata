var sqsUrl = 'https://sqs.eu-west-1.amazonaws.com/828793526013/presentationBuilder-production';
var sqs, AWS = require ( 'aws-sdk' );

var payload = JSON.stringify ( {
    feed: 'livenowentry',
    fixtureId: process.argv[2]
} );

//payload[process.argv[3]] = process.argv[4];

AWS.config.loadFromPath ( './awsConfig.json' );

sqs = new AWS.SQS ();

sqs.sendMessage ( {
    MessageBody: payload,
    QueueUrl: sqsUrl,
    DelaySeconds: 0
}, function ( error, data ) {
    if ( error ) {
        console.log ( error );
        return;
    }

    console.log ( data );
} );
