var apiUrl = 'http://localhost:5000';
var _ = require ( 'lodash' );
var request = require ( 'request' );

request.get ( apiUrl + '/fixtures/noArticleId', function ( error, response, body ) {
    var parsedBody, contentToPush;

    if ( error ) {
        console.log ( error );
        return;
    }

    if ( response.statusCode !== 200 ) {
        console.log ( 'HTTP status code ' + response.statusCode + ' returned' );
        console.log ( body );
        return;
    }

    try {
        parsedBody = JSON.parse ( body );
    } catch ( e ) {
        console.log ( e );
        return;
    }

    contentToPush = _.map ( parsedBody.fixtures.content, function ( fixture ) {
        return {
            id: fixture.id,
            articleId: _.random ( 0, 65535 )
        };
    } );

    request.post ( {
        url: apiUrl + '/fixtures',
        headers: {
            'x-meta-update-source': 'escenic'
        },
        body: JSON.stringify ( contentToPush )
    }, function ( error, response, body ) {
        if ( error ) {
            console.log ( error );
            return;
        }

        if ( response.statusCode !== 200 ) {
            console.log ( 'HTTP status code ' + response.statusCode );
            return;
        }

        console.log ( body );
    } );
} );
