var config = {
    //apiUrl: 'http://pulveriser-api-loadtest-65czkz5gqa.elasticbeanstalk.com',
    //apiUrl: 'http://pulveriser-api-prod-m5pkaaqsig.elasticbeanstalk.com',
    //apiUrl: 'http://pulveriser-api-staging-c734eubqyv.elasticbeanstalk.com',
    apiUrl: 'http://localhost:5000',
    xmlPath: __dirname + '/xml',
    fixtureIdsToSend: [ 695229 ],
    feedsToSend: [ 'F2', 'F3', 'F9', 'F13m' ],
    feedToDetermineTimeFrom: 'F9',
    timeMultiplier: 10,
    skipSeconds: 2700 /* This is NOT taking account the multiplier, ie real time seconds */
};

var _ = require ( 'underscore' );
var async = require ( 'async' );
var moment = require ( 'moment' );
var request = require ( 'request' );
var fs = require ( 'fs' );

var feeds = [];
var minTime;

var sendOptaFeed = function ( feed ) {
    var xml = fs.readFileSync ( feed.xmlFile );
    var headers = feed.headers;

    delete ( headers['content-length'] );

    request ( { url: config.apiUrl + '/opta', method: 'POST', body: xml.toString (), headers: headers }, function ( error, response, body ) {
        var bodyJson;

        if ( error ) {
            console.log ( error );
        } else {
            if ( response.statusCode === 200 ) {
                bodyJson = JSON.parse ( body );

                if ( bodyJson.success ) {
                    console.log ( 'success' );
                } else {
                    console.log ( bodyJson );
                }
            } else {
                console.log ( 'HTTP code ' + response.statusCode );
            }
        }
    } );
};

var getFeedsObject = function ( callBack ) {
    fs.readdir ( config.xmlPath, function ( err, feedDirs ) {
        var i;

        if ( err ) {
            callBack ( err );
            return;
        }

        async.each ( feedDirs, function ( feedDir, callBack ) {
            if ( _.contains ( config.feedsToSend, feedDir ) ) {
                if ( fs.lstatSync ( config.xmlPath + '/' + feedDir ).isDirectory () ) {
                    console.log ( 'Collecting data from ' + config.xmlPath + '/' + feedDir + ' ...' );

                    fs.readdir ( config.xmlPath + '/' + feedDir, function ( err, fixtureDirs ) {
                        if ( err ) {
                            callBack ( err );
                            return;
                        }

                        async.each ( fixtureDirs, function ( fixtureDir, callBack ) {
                            if ( _.contains ( config.fixtureIdsToSend, parseInt ( fixtureDir, 10 ) ) ) {
                                if ( fs.lstatSync ( config.xmlPath + '/' + feedDir + '/' + fixtureDir ).isDirectory () ) {
                                    fs.readdir ( config.xmlPath + '/' + feedDir + '/' + fixtureDir, function ( err, feedFiles ) {
                                        if ( err ) {
                                            callBack ( err );
                                            return;
                                        }

                                        async.each ( feedFiles, function ( feedFile, callBack ) {
                                            var time, headers, headersFile, xmlFile;

                                            if ( feedFile.match ( '.headers' ) ) {
                                                try {
                                                    headersFile = config.xmlPath + '/' + feedDir + '/' + fixtureDir + '/' + feedFile;
                                                    xmlFile = headersFile.replace ( '.headers', '.xml' );
                                                    headers = JSON.parse ( fs.readFileSync ( headersFile ).toString () );
                                                    time = moment ( headers['x-meta-last-updated'], 'ddd MMM DD HH:mm:ss ? YYYY' ).unix ();

                                                    feeds.push ( {
                                                        feedNumber: feedDir,
                                                        fixtureId: fixtureDir,
                                                        time: time,
                                                        headers: headers,
                                                        xmlFile: xmlFile
                                                    } );
                                                } catch ( e ) {
                                                    console.log ( 'Cannot parse ' + headersFile );
                                                    console.log ( e );
                                                }

                                                callBack ( null );
                                            } else {
                                                callBack ( null );
                                            }
                                        }, callBack );
                                    } );
                                } else {
                                    callBack ( null );
                                }
                            } else {
                                callBack ( null );
                            }
                        }, callBack );
                    } );
                } else {
                    callBack ( null );
                }
            } else {
                callBack ( null );
            }
        }, function ( err ) {
            if ( err ) {
                callBack ( err );
                return;
            }

            callBack ( null, feeds );
        } );
    } );
};

getFeedsObject ( function ( error, feeds ) {
    var feedsByFixtureIdSortedByTime, feedsFromFirstToLastF9, feedsTimeNormalised, feedsTogether, sequencer, sequencerTime = 0;

    if ( error ) {
        console.log ( 'ERROR: ' + error );
        return;
    }

    feedsByFixtureIdSortedByTime = _.object ( _.map ( config.fixtureIdsToSend, function ( fixtureId ) {
        return [ fixtureId, _.sortBy ( _.filter ( feeds, function ( feedItem ) {
            return ( feedItem.fixtureId === fixtureId.toString () );
        } ), function ( item ) {
            return item.time;
        } ) ];
    } ) );

    feedsFromFirstToLastF9 = _.object ( _.map ( config.fixtureIdsToSend, function ( fixtureId ) {
        return [ fixtureId, _.reduceRight ( _.reduce ( feedsByFixtureIdSortedByTime[fixtureId], function ( result, item ) {
            return _.isEmpty ( result ) ? ( ( item.feedNumber === config.feedToDetermineTimeFrom ) ? [ item ] : [] ) : _.flatten ( [ result, item ] );
        }, [] ), function ( result, item ) {
            return _.isEmpty ( result ) ? ( ( item.feedNumber === config.feedToDetermineTimeFrom ) ? [ item ] : [] ) : _.flatten ( [ result, item ] );
        }, [] ).reverse () ];
    } ) );

    feedsTimeNormalised = _.object ( _.map ( feedsFromFirstToLastF9, function ( feed, fixtureId ) {
        var baseTime = feed[0].time;

        return [ fixtureId, _.map ( feed, function ( item ) {
            var itemClone = _.clone ( item );

            itemClone.timeAdjusted = itemClone.time - baseTime;
            return itemClone;
        } ) ];
    } ) );

    feedsTogether = _.sortBy ( _.reduce ( feedsTimeNormalised, function ( result, feed, fixtureId ) {
        return _.flatten ( [ result, feed ] );
    }, [] ), function ( item ) {
        return item.timeAdjusted;
    } );

    sequencer = function ( dontSend ) {
        while ( feedsTogether[0].timeAdjusted <= sequencerTime ) {
            var feed = feedsTogether.shift ();

            if ( _.isUndefined ( dontSend ) ) {
                console.log ( 'Sending File: ' + feed.xmlFile );
                sendOptaFeed ( feed );
                console.log ( 'Next feed in ' + parseInt ( ( feedsTogether[0].timeAdjusted - sequencerTime ) / config.timeMultiplier, 10 ) + ' seconds' );
            } else {
                console.log ( 'Skipping File: ' + feed.xmlFile );
            }
        }

        sequencerTime = sequencerTime + 1;
    };

    for ( sequencerTime = 0; sequencerTime < config.skipSeconds; sequencerTime++ ) {
        sequencer ( true );
    }

    console.log ( 'Next feed in ' + parseInt ( ( feedsTogether[0].timeAdjusted - sequencerTime ) / config.timeMultiplier, 10 ) + ' seconds' );

    setInterval ( sequencer, parseInt ( 1000 / config.timeMultiplier, 10 ) );
} );
