var sqsUrl = 'https://sqs.eu-west-1.amazonaws.com/828793526013/pulveriser-opta-test2';
var sqs, AWS = require ( 'aws-sdk' );
var zlib = require ( 'zlib' );
var inspect = require ( 'eyes' ).inspector ( { maxLength: 0 } );

AWS.config.loadFromPath ( './awsConfig.json' );
sqs = new AWS.SQS ();

sqs.receiveMessage ( {
    QueueUrl: sqsUrl,
    MaxNumberOfMessages: 1
}, function ( error, payload ) {
    if ( error ) {
        console.log ( error );
        return;
    }

    inspect ( payload );
} );
