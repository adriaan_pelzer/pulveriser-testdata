var config = {
    apiUrl: 'http://54.217.155.2:443',
    xmlPath: __dirname + '/xml',
    fixtureIds: [ 695111, 695113, 695115, 695116 ],
    feeds: [ 'F2', 'F3', 'F9', 'F13m' ],
    multiplier: 50,
    startAt: 60
};

var _ = require ( 'underscore' );
var async = require ( 'async' );
var moment = require ( 'moment' );
var request = require ( 'request' );
var fs = require ( 'fs' );

var feeds = [];
var minTime;

var sendOptaFeed = function ( feed, index ) {
    var xml = fs.readFileSync ( feed.xmlFile );
    var headers = feed.headers;

    delete ( headers['content-length'] );

    request ( { url: config.apiUrl + '/opta', method: 'POST', body: xml.toString (), headers: headers }, function ( error, response, body ) {
        var bodyJson;

        if ( error ) {
            console.log ( error );
        } else {
            if ( response.statusCode === 200 ) {
                bodyJson = JSON.parse ( body );

                if ( bodyJson.success ) {
                    console.log ( 'success: feed ' + index );
                } else {
                    console.log ( bodyJson );
                }
            } else {
                console.log ( 'HTTP code ' + response.statusCode );
            }
        }
    } );
};

fs.readdir ( config.xmlPath, function ( err, feedDirs ) {
    var i;

    if ( err ) {
        console.log ( err );
        process.exit ( 1 );
    }

    async.each ( feedDirs, function ( feedDir, callBack ) {
        if ( _.indexOf ( config.feeds, feedDir ) !== -1 ) {
            fs.readdir ( config.xmlPath + '/' + feedDir, function ( err, fixtureDirs ) {
                if ( err ) {
                    callBack ( err );
                    return;
                }

                async.each ( fixtureDirs, function ( fixtureDir, callBack ) {
                    if ( _.indexOf ( config.fixtureIds, parseInt ( fixtureDir, 10 ) ) !== -1 ) {
                        fs.readdir ( config.xmlPath + '/' + feedDir + '/' + fixtureDir, function ( err, feedFiles ) {
                            if ( err ) {
                                callBack ( err );
                                return;
                            }

                            async.each ( feedFiles, function ( feedFile, callBack ) {
                                var time, headers, headersFile, xmlFile;

                                if ( feedFile.match ( '.headers' ) ) {
                                    headersFile = config.xmlPath + '/' + feedDir + '/' + fixtureDir + '/' + feedFile;
                                    xmlFile = headersFile.replace ( '.headers', '.xml' );
                                    headers = JSON.parse ( fs.readFileSync ( headersFile ).toString () );
                                    time = moment ( headers['x-meta-last-updated'] ).unix ();

                                    if ( _.isUndefined ( minTime ) || ( time < minTime ) ) {
                                        minTime = time;
                                    }

                                    feeds.push ( {
                                        time: time,
                                        headers: headers,
                                        xmlFile: xmlFile
                                    } );

                                    callBack ( null );
                                } else {
                                    callBack ( null );
                                }
                            }, callBack );
                        } );
                    } else {
                        callBack ( null );
                    }
                }, callBack );
            } );
        } else {
            callBack ( null );
        }
    }, function ( err ) {
        var i;

        if ( err ) {
            console.log ( err );
            process.exit ( 1 );
        }

        feeds.sort ( function ( a, b ) {
            return a.time - b.time;
        } );

        _.map ( feeds, function ( feed ) {
            var returnFeed = feed;

            returnFeed.time = returnFeed.time - minTime;

            return returnFeed;
        } );

        for ( i = config.startAt; i < feeds.length; i++ ) {
            ( function ( index ) {
                setTimeout ( function () {
                    console.log ( 'Sending feed number ' + index );
                    console.log ( 'Sending ' + feeds[index].xmlFile );

                    sendOptaFeed ( feeds[index], index );
                }, 1000 * ( feeds[index].time - feeds[config.startAt].time ) / config.multiplier );
            } ) ( i );
        }
    } );
} );
