fs = require ( 'fs' );

var processFile = function ( file ) {
    var output = '';
    var outputFile = file.replace ( '.headers', '.rawhdrs' );
    var content = fs.readFileSync ( file );
    var key, json = JSON.parse ( content.toString () );

    for ( key in json ) {
        if ( json.hasOwnProperty ( key ) ) {
            if ( key !== 'content-length' ) {
                output += key + ': ' + json[key] + "\n";
            }
        }
    }

    fs.writeFileSync ( outputFile, output );
};

var readDirRecursive = function ( dir, matchPattern ) {
    var output = [];
    var files = fs.readdirSync ( dir );
    var i, file;

    for ( i = 0; i < files.length; i++ ) {
        file = dir + '/' + files[i];

        if ( file.match ( /^\./ ) ) {
            console.log ( 'skip file "' + file + '"' );
        } else {
            if ( fs.lstatSync ( file ).isDirectory () ) {
                output = output.concat ( readDirRecursive ( file, matchPattern ) );
            } else if ( fs.lstatSync ( file ).isFile () ) {
                if ( file.match ( matchPattern ) ) {
                    output.push ( file );
                }
            } else {
                console.log ( 'file "' + file + '" is not a file or a directory' );
            }
        }
    }

    return output;
};

var i, files = readDirRecursive ( 'xml', '.headers' );

for ( i = 0; i < files.length; i++ ) {
    console.log ( 'Processing ' + files[i] + ' ...' );
    processFile ( files[i] );
}
