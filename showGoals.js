var async = require ( 'async' );
var _ = require ( 'underscore' );
var moment = require ( 'moment' );
var xml2js = require ( 'xml2js' ), parser = new xml2js.Parser ();
var fixtureId = process.argv[2];
var fs = require ( 'fs' );
var inspect = require ( 'eyes' ).inspector ( { maxLength: 0 } );
var dirName = __dirname + '/xml/F9/' + fixtureId;

fs.readdir ( dirName, function ( error, files ) {
    if ( error ) {
        console.log ( error );
        return;
    }

    async.parallel ( _.map ( _.filter ( files, function ( file ) {
        return file.match ( /\.xml$/ );
    } ), function ( file ) {
        return function ( callBack ) {
            var fileName = dirName + '/' + file;

            fs.readFile ( fileName, function ( error, fileContent ) {
                if ( error ) {
                    callBack ( error );
                    return;
                }

                parser.parseString ( fileContent, function ( error, json ) {
                    var goals = [], teamGoals;
                    var time = moment ( json.SoccerFeed.$.TimeStamp, 'YYYYMMDDTHHmmssZZ' );

                    if ( error ) {
                        callBack ( error );
                        return;
                    }

                    teamGoals = json.SoccerFeed.SoccerDocument[0].MatchData[0].TeamData[0].Goal;

                    if ( ! _.isUndefined ( teamGoals ) ) {
                        goals.push ( teamGoals.length );
                    } else {
                        goals.push ( 0 );
                    }

                    teamGoals = json.SoccerFeed.SoccerDocument[0].MatchData[0].TeamData[1].Goal;

                    if ( ! _.isUndefined ( teamGoals ) ) {
                        goals.push ( teamGoals.length );
                    } else {
                        goals.push ( 0 );
                    }

                    callBack ( null, {
                        time: time,
                        goals: goals,
                        fileName: fileName
                    } );
                } );
            } );
        };
    } ), function ( error, results ) {
        var goalFeeds;

        if ( error ) {
            console.log ( error );
            return;
        }

        goalFeeds = _.reduce ( _.sortBy ( results, function ( result ) {
            return result.time;
        } ), function ( goalsScored, feed ) {
            if ( ! _.isEqual ( goalsScored.previous, feed.goals ) ) {
                goalsScored.result.push ( feed.fileName );
            }

            goalsScored.previous = feed.goals;

            return goalsScored;
        }, {
            previous: [ 0, 0 ],
            result: []
        } );

        inspect ( goalFeeds );
    } );
} );
