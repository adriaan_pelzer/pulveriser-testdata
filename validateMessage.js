var pathPortion = process.argv[2];
var headerFile = pathPortion + '.headers';
var xmlFile = pathPortion + '.xml';
var fs = require ( 'fs' );
var crypto = require ( 'crypto' );
var md5sum = crypto.createHash ( 'md5' );

var header = JSON.parse ( fs.readFileSync ( headerFile ) );
var xml = fs.readFileSync ( xmlFile );

console.log ( header['x-meta-message-digest'] );
console.log ( md5sum.update ( xml ).digest ( 'hex' ) );
