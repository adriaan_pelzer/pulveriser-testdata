var apiUrl = 'http://localhost:8000';

var request = require ( 'request' );

var fs = require ( 'fs' );

var filePrefix = process.argv[2];

var xml = fs.readFileSync ( filePrefix + '.xml' );

var headers = JSON.parse ( fs.readFileSync ( filePrefix + '.headers' ) );

delete ( headers['content-length'] );

request ( { url: apiUrl + '/opta', method: 'POST', body: xml.toString (), headers: headers }, function ( error, response, body ) {
    var bodyJson;
    
    if ( error ) {
        console.log ( error );
    } else {
        if ( response.statusCode === 200 ) {
            bodyJson = JSON.parse ( body );

            if ( bodyJson.success ) {
                console.log ( 'success' );
            } else {
                console.log ( bodyJson );
            }
        } else {
            console.log ( 'HTTP code ' + response.statusCode );
        }
    }
} );
