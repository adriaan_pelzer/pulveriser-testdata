var mysql = require ( 'mysql' );
var fs = require ( 'fs' );
var async = require ( 'async' );

var connection = mysql.createConnection ( {
    host: 'localhost',
    user: 'test',
    password: 'test',
    database: 'endavaTest'
} );

connection.connect ( function ( err ) {
    var queryFactory = function ( table ) {
        return function ( callBack ) {
            var query = 'DELETE FROM `' + table + '` WHERE 1;';
            console.log ( query );
            connection.query ( query, callBack );
        };
    };

    if ( err ) {
        console.log ( err );
        process.exit ( 1 );
    }

    async.auto ( { 
        commentary: [ queryFactory ( 'commentary' ) ],
        url: [ queryFactory ( 'url' ) ],
        'fixture-team': [ queryFactory ( 'fixture-team' ) ],
        player: [ 'team-event', queryFactory ( 'player' ) ],
        'team-event': [ queryFactory ( 'team-event' ) ],
        teamstats: [ queryFactory ( 'teamstats' ) ],
        fixture: [ 'commentary', 'url', 'fixture-team', 'team-event', queryFactory ( 'fixture' ) ],
        team: [ 'fixture', 'teamstats', 'team-event', 'player', 'fixture-team', queryFactory ( 'team' ) ]
    },function ( err, results ) {
        var i;

        if ( err ) {
            console.log ( err );
        } else {
            for ( i in results ) {
                if ( results.hasOwnProperty ( i ) ) {
                    if ( results[i][0].serverStatus !== 34 ) {
                        console.log ( 'Cannot delete ' + i + ': serverStatus is ' + results[i][0].serverStatus );
                    }
                }
            }
        }

        connection.end ();
    } );
} );
        
fs.readdir ( '/home/ubuntu/data-test', function ( err, files ) {
    if ( err ) {
        console.log ( 'Cannot read directory' );
        console.log ( err );
        process.exit ( 1 );
    }

    async.each ( files, function ( file, callBack ) {
        if ( file.match ( '.json' ) ) {
            fs.unlink ( '/home/ubuntu/data-test/' + file, callBack );
        } else {
            callBack ( null );
        }
    }, function ( err ) {
        if ( err ) {
            console.log ( 'Cannot unlink file' );
            console.log ( err );
        }
    } );
} );
