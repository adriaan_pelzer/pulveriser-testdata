var sqsUrl = 'https://sqs.eu-west-1.amazonaws.com/828793526013/pulveriser-opta-test2';
var sqs, AWS = require ( 'aws-sdk' );
var fs = require ( 'fs' );
var filePrefix = process.argv[2];
var xml = fs.readFileSync ( filePrefix + '.xml' ).toString ();
var headers = JSON.parse ( fs.readFileSync ( filePrefix + '.headers' ).toString () );
var zlib = require ( 'zlib' );

delete ( headers['content-length'] );

var payload = JSON.stringify ( {
    xml: xml,
    headers: headers
} );

AWS.config.loadFromPath ( './awsConfig.json' );

zlib.deflate ( payload, function ( error, buffer ) {
    sqs = new AWS.SQS ();

    sqs.sendMessage ( {
        MessageBody: buffer.toString ( 'base64' ),
        QueueUrl: sqsUrl,
        DelaySeconds: 0
    }, function ( error, data ) {
        if ( error ) {
            console.log ( error );
            return;
        }

        console.log ( data );
    } );
} );
