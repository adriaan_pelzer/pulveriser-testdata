var sqsUrl = 'https://sqs.eu-west-1.amazonaws.com/828793526013/presentationBuilder-test';
var sqs, AWS = require ( 'aws-sdk' );

AWS.config.loadFromPath ( './awsConfig.json' );

sqs = new AWS.SQS ();

sqs.sendMessage ( {
    MessageBody: JSON.stringify ( {
        feed: process.argv[2],
        fixtureId: process.argv[3]
    } ),
    QueueUrl: sqsUrl,
    DelaySeconds: 0
}, function ( error, data ) {
    if ( error ) {
        console.log ( error );
        return;
    }

    console.log ( data );
} );
