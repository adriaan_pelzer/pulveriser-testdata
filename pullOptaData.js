var mysql = require ( 'mysql' );
var async = require ( 'async' );
var fs = require ( 'fs' );

var conn = mysql.createConnection ( {
    host: 'localhost',
    user: 'opta',
    password: 'opta',
    database: 'opta'
} );

var B64decodeString = function ( stringEncoded ) {
    var buffer = new Buffer ( stringEncoded, 'base64' );
    var string_decoded = buffer.toString ( 'utf8' );

    return string_decoded;
};

var decodeObject = function ( objectEncoded ) {
    var objectDecoded_string = B64decodeString ( objectEncoded );
    var object = JSON.parse ( objectDecoded_string );

    return object;
};

var folder = process.argv[2];

var readDirRecursive = function ( dir, matchPattern ) {
    var output = [];
    var files = fs.readdirSync ( dir );
    var i, file;

    for ( i = 0; i < files.length; i++ ) {
        file = dir + '/' + files[i];

        if ( ! file.match ( /^\./ ) ) {
            if ( fs.lstatSync ( file ).isDirectory () ) {
                output = output.concat ( readDirRecursive ( file, matchPattern ) );
            } else if ( fs.lstatSync ( file ).isFile () ) {
                if ( file.match ( matchPattern ) ) {
                    output.push ( file );
                }
            } else {
                console.log ( 'file "' + file + '" is not a file or a directory' );
            }
        }
    }

    return output;
};

var getMax = function ( files ) {
    var i, candidate, max = 0, pathParts, fileParts;
    
    for ( i = 0; i < files.length; i++ ) {
        pathParts = files[i].split ( '/' );
        fileParts = pathParts[pathParts.length - 1].split ( '.' );
        candidate = parseInt ( fileParts[0], 10 );
        if ( candidate > max ) {
            max = candidate;
        }
    }

    return max;
};

var maxId = getMax ( readDirRecursive ( folder, '.xml' ) );
conn.connect ();

conn.query ( 'SELECT COUNT(*) FROM `opta` WHERE `id`>' + maxId, function ( err, rows ) {
    var i, queries = [], rowCount, pageSize = 1000;

    if ( err ) {
        console.log ( err );
        process.exit ( 1 );
    }

    rowCount = parseInt ( rows[0]['COUNT(*)'], 10 );

    for ( i = 0; i < rowCount; i += pageSize ) {
        ( function ( index ) {
            queries.push ( function ( callBack ) {
                var query = 'SELECT * FROM `opta` WHERE `id`>' + maxId + ' LIMIT ' + index + ', ' + pageSize;

                conn.query ( query, function ( err, rows ) {
                    var headers, body, j;

                    if ( err ) {
                        callBack ( err );
                    } else {
                        for ( j = 0; j < rows.length; j++ ) {
                            headers = decodeObject ( rows[j].headers );
                            body = B64decodeString ( rows[j].body ).replace ( /\\n/g, "\n").replace ( /\\"/g, "\"" ).replace ( /^"/, '' ).replace ( /"$/, "" );

                            try {
                                fs.mkdirSync ( folder + '/' + headers['x-meta-feed-type'] );
                            } catch ( e ) {
                                if ( e.code !== 'EEXIST' ) {
                                    console.log ( e );
                                }
                            }

                            try {
                                fs.mkdirSync ( folder + '/' + headers['x-meta-feed-type'] + '/' + headers['x-meta-game-id'] );
                            } catch ( e ) {
                                if ( e.code !== 'EEXIST' ) {
                                    console.log ( e );
                                }
                            }

                            try {
                                fs.writeFileSync ( folder + "/" + headers['x-meta-feed-type'] + '/' + headers['x-meta-game-id'] + '/' + rows[j].id + ".xml", body );
                                console.log ( "./xml/" + headers['x-meta-feed-type'] + '/' + headers['x-meta-game-id'] + '/' + rows[j].id + ".xml written" );
                            } catch ( e ) {
                                console.log ( e );
                            }

                            try {
                                fs.writeFileSync ( folder + "/" + headers['x-meta-feed-type'] + '/' + headers['x-meta-game-id'] + '/' + rows[j].id + ".headers", JSON.stringify ( headers ) );
                                console.log ( "./xml/" + headers['x-meta-feed-type'] + '/' + headers['x-meta-game-id'] + '/' + rows[j].id + ".headers written" );
                            } catch ( e ) {
                                console.log ( e );
                            }
                        }

                        callBack ( null );
                    }
                } );
            } );
        } ) ( i );
    }

    async.series ( queries, function ( err, results ) {
        var rows, headers, body;

        if ( err ) {
            console.log ( err );
            process.exit ( 1 );
        }
    } );
} );
